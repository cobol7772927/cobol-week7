       IDENTIFICATION DIVISION.
       PROGRAM-ID. SALE-REPORT.
       AUTHOR. Kittipon Thaweelarb.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "FOODSALE.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS  0 RECORDS.
      *    This is a record buffer
      *    the storage when you use "100-INPUT-FILE" to read a data from file
       01 100-INPUT-RECORD.
          05 BRANCH                PIC X(1).
          05 FILLER                PIC X(7).
          05 FOOD-ID               PIC X(4).
          05 FILLER                PIC X(8).
          05 FOOD-NAME             PIC X(23).
          05 FOOD-PRICE            PIC X(4).
          05 FILLER                PIC X(5).
          05 FOOD-QUANTITY         PIC X(1).
          05 FILLER                PIC X(7).
          05 SALE-DATE.      
             10 SALE-YEAR          PIC 9(4).
             10 FILLER             PIC X(1).
             10 SALE-MONTH         PIC 9(2).
             10 FILLER             PIC X(1).
             10 SALE-DAY           PIC 9(2).
          05 FILLER                PIC X(1).
          05 SALE-TIME             PIC X(5).
          05 FILLER                PIC X(4).

           
       WORKING-STORAGE SECTION. 
      *01 WS-INPUT-FILE-STATUS     PIC X(2).
      *   88 FILE-OK                         VALUE '00'.
      *   88 FILE-AT-END                     VALUE '10'.
      COPY "WS00293.CPY" REPLACING  WS00293 BY WS-INPUT-FILE-STATUS.
       01 WS-CALCULATION.
          05 WS-INPUT-COUNT        PIC 9(5).
          05 WS-SALE-SUMMARY.
             10 WS-TOTAL-PRICE     PIC 9(8)  VALUE ZERO.
             10 WS-TOTAL-BRANCH-A  PIC 9(8)  VALUE ZERO.
             10 WS-TOTAL-BRANCH-B  PIC 9(8)  VALUE ZERO.
             10 WS-TOTAL-BRANCH-C  PIC 9(8)  VALUE ZERO.
          05 WS-SALE.
             10 WS-BRANCH          PIC X(1).
             10 WS-FOOD-ID         PIC X(4).
             10 WS-FOOD-NAME       PIC X(23).
             10 WS-FOOD-PRICE      PIC 9(4).
             10 WS-FOOD-QUANTITY   PIC 9(1).
             10 WS-SALE-DATE.      
                15 WS-SALE-YEAR    PIC 9(4).
                15 WS-SALE-MONTH   PIC 9(2).
                15 WS-SALE-DAY     PIC 9(2).
             10 WS-SALE-TIME       PIC X(5).
       PROCEDURE DIVISION .
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT.
           PERFORM 2000-PROCESS THRU 2000-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS.
           PERFORM 3000-END THRU 3000-EXIT.
           GOBACK.

       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE
      *    check error
           IF FILE-OK THEN
              DISPLAY "Read file success ;)"
              CONTINUE 
           ELSE
              DISPLAY "***** SALE-REPORT ABEND *****"
                 UPON CONSOLE 
              DISPLAY "* PARA 1000-INITIAL FAILED *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS = " WS-INPUT-FILE-STATUS " *"
                 UPON CONSOLE
              DISPLAY "***** SALE-REPORT ABEND *****"
              STOP RUN 
           END-IF

      *    when you read a file the data in a file is available on "record buffe
           PERFORM 8000-READ-FILE-INPUT THRU 8000-EXIT.
       1000-EXIT.
           EXIT.
       
       2000-PROCESS.
      *    then display record on "record buffer"
           PERFORM 2100-MOVE-SALE THRU 2100-EXIT 
           PERFORM 2200-SUM-TOTAL-SALE THRU 2200-EXIT
           DISPLAY WS-SALE 
           PERFORM 8000-READ-FILE-INPUT THRU 8000-EXIT
           .
       2000-EXIT.
           EXIT.

       2100-MOVE-SALE.
           MOVE BRANCH OF 100-INPUT-RECORD
              TO WS-BRANCH 
           MOVE FOOD-ID OF 100-INPUT-RECORD
              TO WS-FOOD-ID 
           MOVE FOOD-NAME OF 100-INPUT-RECORD
              TO WS-FOOD-NAME 
           MOVE FOOD-PRICE OF 100-INPUT-RECORD
              TO WS-FOOD-PRICE 
           MOVE FOOD-QUANTITY OF 100-INPUT-RECORD
              TO WS-FOOD-QUANTITY 
           MOVE SALE-YEAR OF 100-INPUT-RECORD
              TO WS-SALE-YEAR
           MOVE SALE-DAY OF 100-INPUT-RECORD
              TO WS-SALE-DAY  
           MOVE SALE-MONTH OF 100-INPUT-RECORD
              TO WS-SALE-MONTH
           MOVE SALE-TIME OF 100-INPUT-RECORD
              TO WS-SALE-TIME 
           .
       2100-EXIT.
           EXIT. 
       
       2200-SUM-TOTAL-SALE.
           COMPUTE WS-TOTAL-PRICE =
              WS-TOTAL-PRICE +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
      *    IF WS-BRANCH = "A"
      *       COMPUTE WS-TOTAL-BRANCH-A =
      *          WS-TOTAL-BRANCH-A +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
      *    ELSE
      *       IF WS-BRANCH = "B"
      *          COMPUTE WS-TOTAL-BRANCH-B =
      *             WS-TOTAL-BRANCH-B +(WS-FOOD-PRICE * WS-FOOD-QUANTITY
      *              )
      *       ELSE
      *          IF WS-BRANCH = "C"
      *             COMPUTE WS-TOTAL-BRANCH-C =
      *                WS-TOTAL-BRANCH-C +(WS-FOOD-PRICE * WS-FOOD-QUANT
      *                ITY)
      *          END-IF
      *       END-IF
      *    END-IF
           EVALUATE WS-BRANCH 
           WHEN "A"
                COMPUTE WS-TOTAL-BRANCH-A =
                   WS-TOTAL-BRANCH-A +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           WHEN "B"
                COMPUTE WS-TOTAL-BRANCH-B =
                   WS-TOTAL-BRANCH-B +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           WHEN "C"
                COMPUTE WS-TOTAL-BRANCH-C =
                   WS-TOTAL-BRANCH-C +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           END-EVALUATE 
           .
       2200-EXIT.
           EXIT.
       3000-END.
      *    after you open the file you must close the file
           CLOSE 100-INPUT-FILE 
      *    check error
           IF FILE-OK THEN
              CONTINUE 
           ELSE
              DISPLAY "***** SALE-REPORT ABEND *****"
                 UPON CONSOLE 
              DISPLAY "* PARA 3000-END FAILED *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS = " WS-INPUT-FILE-STATUS " *"
                 UPON CONSOLE
              DISPLAY "***** SALE-REPORT ABEND *****"
              STOP RUN 
           END-IF
           
           DISPLAY "READ " WS-INPUT-COUNT " RECORDS"
           DISPLAY "total price " WS-TOTAL-PRICE 
           DISPLAY "total price on branch A " WS-TOTAL-BRANCH-A  
           DISPLAY "total price on branch B " WS-TOTAL-BRANCH-B  
           DISPLAY "total price on branch C " WS-TOTAL-BRANCH-C   
           .
       3000-EXIT.
           EXIT.

       8000-READ-FILE-INPUT.
           READ 100-INPUT-FILE
      *    check error
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD +1 TO WS-INPUT-COUNT
           ELSE
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS
                 CONTINUE
              ELSE
                 DISPLAY "***** SALE-REPORT ABEND *****"
                    UPON CONSOLE 
                 DISPLAY "* PARA 8000-READ-FILE-INPUT FAILED *"
                    UPON CONSOLE
                 DISPLAY "* FILE STATUS = " WS-INPUT-FILE-STATUS " *"
                    UPON CONSOLE
                 DISPLAY "***** SALE-REPORT ABEND *****"
                    UPON CONSOLE   
                 STOP RUN
              END-IF 
           END-IF 
           .
       8000-EXIT.
           EXIT.